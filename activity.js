// 1. What directive is used by Node.js in loading the modules it needs?

The require directive.

// 2. What Node.js module contains a method for server creation?

The HTTP module.

// 3. What is the method of the http object responsible for creating a server using Node.js?

The createServer() method.

// 4. What method of the response object allows us to set status codes and content types?
	
The writeHead() method.

// 5. Where will console.log() output its contents when run in Node.js?

In the git bash console after launching the server.

// 6. What property of the request object contains the address's endpoint?

The request.url
