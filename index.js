let http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {

	if( request.url == "/login" ){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("You are at the login page. Welcome!");
	} else{
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("I'm sorry, the page you are looking for is unavailable.");
	}

});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}`);